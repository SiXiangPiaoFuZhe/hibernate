package com.zhanghe.study;

import com.zhanghe.study.model.User;
import com.zhanghe.study.model.many2one.Customer;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.service.ServiceRegistry;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * @author zh
 * @date 2021/1/16 17:47
 */
public class TestMain {

    private SessionFactory sessionFactory;

    @Before
    public void beforeStart(){
        try {
            // 对应的hibernate基本配置信息和对象关系映射信息
            Configuration configuration = new Configuration();
            // 找的hibernate.cfg.xml配置文件
            configuration.configure();
            // 4.0之前不需要使用serviceRegistry  直接构建sessionFactory  configuration.buildSessionFactory()
            // 4.0之后创建serviceRegistry,所有的配置和服务都要在该对象中注册才可以生效
            // 4.3之前使用该方式new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }


    // 创建session对象
    public Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    /**
     * 测试save方法
     */
    @Test
    public void testSave(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = new User();
            user.setAge(18);
            user.setName("李四");
            user.setId(100);
            session.save(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }
    }

    /**
     * 测试persist方法
     */
    @Test
    public void testPersist(){

        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = new User();
            user.setAge(18);
            user.setName("李四");
            user.setId(100);
            session.persist(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }

    }

    /**
     * 测试get方法
     */
    @Test
    public void testGet(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = (User) session.get(User.class,100);
            System.out.println(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }

    }

    /**
     * 测试load方法
     */
    @Test
    public void testLoad(){

        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = (User) session.load(User.class,1);
            System.out.println(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }

    }

    @Test
    public void testOid(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = (User) session.get(User.class,1);
            System.out.println(user);
            User user1  = new User();
            user1.setId(1);
            user1.setName("王五");
            session.saveOrUpdate(user1);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }

    }

    @Test
    public void testEvict(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            User user = (User) session.get(User.class,1);
            user.setName("张三1");
            session.evict(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }

    @Test
    public void testConnection(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.doWork(new Work() {
                @Override
                public void execute(Connection connection) throws SQLException {

                }
            });

            session.doReturningWork(new ReturningWork<Object>() {
                @Override
                public Object execute(Connection connection) throws SQLException {
                    return null;
                }
            });
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }

    }

    @Test
    public void manyToOne(){
        Customer customer = new Customer();
    }

    /**
     * 使用参数位置
     */
    @Test
    public void testQuery(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            String hql = "from User where id > ?";
            List<User> users = session.createQuery(hql)
                    .setInteger(0,2).list();
            System.out.println(users);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }

    }

    /**
     * 使用参数名称
     */
    @Test
    public void testQuery2(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            String hql = "from User where id > :id";
            List<User> users = session.createQuery(hql)
                    .setInteger("id",2).list();
            System.out.println(users);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }

    /**
     * 分页
     */
    @Test
    public void testPage(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            List<User> users = session.createQuery("from User")
                    .setFirstResult(1)
                    .setMaxResults(2).list();
            System.out.println(users);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }

    /**
     * 查询部分字段
     */
    @Test
    public void testFieldQuery(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            List<Object[]> result = session.createQuery("select id,name from User").list();
            for(Object[] objs : result){
                System.out.println(Arrays.asList(objs));
            }
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }

    /**
     * 查询部分字段
     */
    @Test
    public void testFieldQuery2(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            List<User> result = session.createQuery("select new User(id,name) from User").list();
            for(User user : result){
                System.out.println(user);
            }
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }

    /**
     * 测试Criteria
     */
    @Test
    public void testCriteria(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("id",2));
            User user = (User) criteria.uniqueResult();
            System.out.println(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }

    /**
     * 测试Criteria
     */
    @Test
    public void testCriteria2(){
        // 开启事务
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try{
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("id",2));
            // 分页
//            criteria.setFirstResult(0);
//            criteria.setMaxResults(10);
            User user = (User) criteria.uniqueResult();
            System.out.println(user);
            System.out.println(user);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            e.printStackTrace();
        } finally {
            if(session != null && session.isOpen()){
                session.close();
            }
            if(sessionFactory != null && !sessionFactory.isClosed()){
                sessionFactory.close();
            }
        }


    }
}
