package com.zhanghe.study.model.one2one;

/**
 * @author zh
 * @date 2020/12/7 09:29
 */
public class Department {
    private int id;
    private String deptName;
    private Manager manager;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }
}
