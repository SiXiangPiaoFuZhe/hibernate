package com.zhanghe.study.model.one2one;

/**
 * @author zh
 * @date 2020/12/7 09:32
 */
public class Manager {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
