package com.zhanghe.study.model.many2one;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zh
 * @date 2020/12/5 22:24
 */
public class Customer {
    private int id;

    private String customerName;

    private List<Order> orderList = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

}
